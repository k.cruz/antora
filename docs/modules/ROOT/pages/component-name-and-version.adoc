= Define a Component Version
//Define a Component Name and Version
:note-caption: ASSUMPTIONS

[NOTE]
====
You understand what a xref:component-version.adoc[component version] is and the purpose of xref:component-version-descriptor.adoc[_antora.yml_].
====

On this page, you'll learn:

* [x] The requirements an [.path]_antora.yml_ file must meet.
* [x] How to assign a valid value to the `name` key.
* [x] How to assign a valid value to the `version` key.
* [x] How to enter keys and values in an [.path]_antora.yml_ file.

[#antora-yml-requirements]
== antora.yml requirements

A component version descriptor file must:

* be named [.path]_antora.yml_
* be written in valid YAML
* contain the `name` key and its value
* be stored xref:module-directories.adoc[at the same directory level as the _modules_ directory]

The following section provides instructions for creating an [.path]_antora.yml_ file and assigning values to the `name` and `version` keys to define a component version.
For detailed information about the purpose of `name` and `version`, how Antora uses them, their specific requirements, and more usage examples, see xref:component-name-key.adoc[] and xref:component-version-key.adoc[].

[#assign-name-and-version]
== Assign a name and version to a component version

Let's create an [.path]_antora.yml_ file that defines the name and version of a component version.
In this exercise, we'll create a component version for the project _Silver Leaf_.
Its component name will be _silver-leaf_, and its version will be _7.1_.

. Open a new file in the text editor or IDE of your choice.
. On the first line, type `name`, directly followed by a colon (`:`).
. Press the kbd:[Spacebar] to insert a space after the colon, and then type the value you want to assign to `name`.
The `name` key doesn't accept certain characters or spaces in its value, see xref:component-name-key.adoc#requirements[name requirements] for details.
+
[source,yaml]
----
name: silver-leaf
----

. At the end of the value, press kbd:[Enter] to go to the next line.
. Type `version`, directly followed by a colon (`:`), and then press the kbd:[Spacebar] to insert a space.
. Type the value you want to assign to `version`.
In this example, the value is enclosed in a set of single quote marks (`'`) because it starts with a number.
+
[source,yaml]
----
name: silver-leaf
version: '7.1'
----
+
The `version` key doesn't accept certain characters or spaces in its value, see xref:component-version-key.adoc#requirements[version requirements] for details.
. Save the file as [.path]_antora.yml_ in the xref:content-source-repositories.adoc#content-source-root[content source root].
The [.path]_antora.yml_ file should be located at the same hierarchy level as the [.path]_modules_ directory to which you want it applied.

You've now associated a set of source files with a component version!
When Antora runs, all of the xref:standard-directories.adoc[source files stored in the standard set of directories] will be assigned the component name `silver-leaf` and the version `7.1`.
These values will be used as xref:page:page-id.adoc[page] and xref:page:resource-id.adoc[resource ID] coordinates and in the URLs of the pages generated from the component version's source files.

[#optional-keys]
== Optional antora.yml keys

You can assign additional metadata, designate a component version as a prerelease, apply AsciiDoc attributes, and register a component version's navigation list and start page using the component version descriptor.

include::partial$optional-component-version-keys.adoc[]
